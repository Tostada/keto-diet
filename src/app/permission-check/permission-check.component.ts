import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  catchError,
  debounceTime,
  Observable,
  of,
  Subscription,
  tap,
} from 'rxjs';
import { GetCheckResponse, InternalRelationTuple } from '@ory/keto-client';
import { KetoReadService } from '../services/keto-read.service';

@Component({
  selector: 'app-permission-check',
  templateUrl: './permission-check.component.html',
  styleUrls: ['./permission-check.component.scss'],
})
export class PermissionCheckComponent implements OnInit {
  @Input() relationTuple: InternalRelationTuple | undefined;
  @Output() relationTupleChange = new EventEmitter<InternalRelationTuple>();

  readonly relationQuery = new FormGroup<RelationTupleForm>({
    namespace: new FormControl('', { nonNullable: true }),
    object: new FormControl('', { nonNullable: true }),
    relation: new FormControl('', { nonNullable: true }),
    subject_id: new FormControl('', { nonNullable: true }),
    /*subject_set: new FormGroup({
      namespace: new FormControl('', { nonNullable: true }),
      object: new FormControl('', { nonNullable: true }),
      relation: new FormControl('', { nonNullable: true }),
    }),*/
  });

  relationTupleStyle: 'id' | 'set' = 'id';
  checkResponse$?: Observable<GetCheckResponse>;

  subscriptions = new Subscription();
  constructor(private readonly ketoReadService: KetoReadService) {
    this.subscriptions.add(
      this.relationQuery.valueChanges
        .pipe(
          debounceTime(500),
          tap(() =>
            this.relationTupleChange.emit(this.relationQuery.getRawValue())
          )
        )
        .subscribe()
    );
  }

  ngOnInit(): void {
    if (this.relationTuple)
      this.relationQuery.patchValue(this.relationTuple, { emitEvent: false });
  }

  check(queryForm: FormGroup<RelationTupleForm>) {
    this.checkResponse$ = this.ketoReadService
      .check(queryForm.getRawValue())
      .pipe(
        catchError((err, caught) => {
          return of({ allowed: false });
        })
      );
  }

  getLetter(allowed: boolean | undefined) {
    if (allowed == undefined) return '';

    if (allowed) return 'A';

    return 'D';
  }

  getClass(allowed: boolean | undefined) {
    if (allowed == undefined) return 'gray';

    if (allowed) return 'green';

    return 'red';
  }
}

interface RelationTupleForm extends SubjectSetForm {
  subject_id?: FormControl<string>;
  subject_set?: FormGroup<SubjectSetForm>;
}

interface SubjectSetForm {
  namespace: FormControl<string>;
  object: FormControl<string>;
  relation: FormControl<string>;
}
