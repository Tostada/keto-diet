import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  readonly routes = [
    {
      href: '',
      title: 'Dashboard',
      icon: 'dashboard',
    },
    {
      href: 'explore',
      title: 'Explore',
      icon: 'list_alt',
    },
    {
      href: 'check',
      title: 'Check',
      icon: 'check',
    },
    {
      href: 'settings',
      title: 'Settings',
      icon: 'settings',
    },
    {
      href: 'expand',
      title: 'Expand',
      icon: 'expand_content',
    },
  ];
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
