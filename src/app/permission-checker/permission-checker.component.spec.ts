import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionCheckerComponent } from './permission-checker.component';

describe('PermissionCheckerComponent', () => {
  let component: PermissionCheckerComponent;
  let fixture: ComponentFixture<PermissionCheckerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermissionCheckerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PermissionCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
