import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnDestroy } from '@angular/core';
import { InternalRelationTuple } from '@ory/keto-client';

@Component({
  selector: 'app-permission-checker',
  templateUrl: './permission-checker.component.html',
  styleUrls: ['./permission-checker.component.scss'],
})
export class PermissionCheckerComponent implements OnDestroy {
  checks: InternalRelationTuple[] = [];
  constructor() {
    this.checks = JSON.parse(localStorage.getItem('checks') ?? '[]') ?? [];
  }

  addRequest() {
    this.checks.push({ namespace: '', object: '', relation: '' });
    this.saveStorage();
  }

  removeRequest(index: number) {
    this.checks.splice(index, 1);
    this.saveStorage();
  }

  ngOnDestroy(): void {
    this.saveStorage();
  }

  saveStorage() {
    localStorage.setItem('checks', JSON.stringify(this.checks));
  }

  trackByIndex(index: number, item: InternalRelationTuple) {
    return item;
  }

  drop(event: CdkDragDrop<InternalRelationTuple[]>) {
    moveItemInArray(this.checks, event.previousIndex, event.currentIndex);
    this.saveStorage();
  }
}
