import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { TupleExplorerComponent } from './tuple-explorer/tuple-explorer.component';
import { PermissionCheckerComponent } from './permission-checker/permission-checker.component';
import { ExpandComponent } from './expand/expand.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'explore', component: TupleExplorerComponent },
  { path: 'check', component: PermissionCheckerComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'expand', component: ExpandComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
