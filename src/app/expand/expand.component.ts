import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KetoReadService } from '../services/keto-read.service';
import * as cytoscape from 'cytoscape';
import { ExpandTree } from '@ory/keto-client';
import { Subscription, debounceTime, finalize } from 'rxjs';

@Component({
  selector: 'app-expand',
  templateUrl: './expand.component.html',
  styleUrls: ['./expand.component.scss'],
})
export class ExpandComponent implements OnInit, OnDestroy, AfterViewInit {
  private readonly expandFormKey = 'expandFormData';
  @ViewChild('cy') cy!: ElementRef<HTMLElement>;

  formGroup!: FormGroup;
  cyto: cytoscape.Core;
  queryKeys = ['namespace', 'object', 'relation', 'max_depth'];
  layoutConfig: cytoscape.LayoutOptions = {
    name: 'concentric',
    nodeDimensionsIncludeLabels: true,
    //minNodeSpacing: 0.1,
    minNodeSpacing: 100,
    directed: true,
    /* depthSort: function (a, b) {
      return a.data('weight') - b.data('weight');
    }, */
    //spacingFactor: 1.25,
    /* transform: (node, position) => {
      if (node.data('type') == 'subject-set') {
        //console.log(node);
        //node.outgoers().layout(this.layoutConfig).run();
        return {
          x: node.cy().$id('root').position().x,
          y: position.y + 300,
        };
      }

      return position;
    }, */
    //maximal: true,
  };

  private readonly subscriptions = new Subscription();

  constructor(private readonly ketoRead: KetoReadService) {
    this.cyto = cytoscape({
      style: [
        // the stylesheet for the graph
        {
          selector: 'node[label]',
          style: {
            'background-opacity': 0.8,
            'background-color': '#7B68EE',
            width: '3em',
            height: '3em',
            color: '#7B68EE',
            label: 'data(label)',
            'font-size': '2em',
          },
        },
        {
          selector: 'node[type="root"]',
          style: {
            'background-color': '#EE82EE',
            color: '#EE82EE',
            shape: 'star',
          },
        },
        {
          selector: 'node[type="subject"]',
          style: {
            'background-color': '#444',
            'background-opacity': 0.75,
            color: '#7FFFD4',
            opacity: 0.9,
            shape: 'round-rectangle',
            'text-halign': 'center',
            'text-valign': 'center',
            'min-zoomed-font-size': 16,
            width: 'label',
            height: 'label',
            'padding-left': '0.25em',
            'padding-right': '0.5em',
            'padding-top': '0.5em',
            'font-size': '2em',
          },
        },
        {
          selector: 'edge',
          style: {
            width: 1,
            'line-color': '#ccc',
            'target-arrow-color': '#ccc',
            'target-arrow-shape': 'triangle',
            'curve-style': 'bezier',
            color: '#aaa',
            opacity: 0.25,
          },
        },
        {
          selector: 'edge[type="hierarchy"]',
          style: {
            width: 2,
            'line-color': '#FF4500',
            'target-arrow-color': '#ccc',
            'target-arrow-shape': 'triangle',
            'curve-style': 'bezier',
            color: '#aaa',
            opacity: 0.6,
          },
        },
        {
          selector: '[type="invisible"]',
          style: {
            //opacity: 0,
          },
        },
      ],
      wheelSensitivity: 0.125,
    });

    /* this.cyto.on('tap', (event) => {
      if (event.target === this.cyto) {
      } else {
        console.log(event.target);
        //const node = event.target;
      }
    }); */
  }

  ngAfterViewInit(): void {
    this.cyto.mount(this.cy.nativeElement);
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      namespace: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required],
      }),
      object: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required],
      }),
      relation: new FormControl('', {
        nonNullable: true,
        validators: [Validators.required],
      }),
      max_depth: new FormControl(3, {
        nonNullable: true,
        validators: [Validators.required],
      }),
      subject: new FormControl(''),
    });

    this.subscriptions.add(
      this.formGroup.valueChanges
        .pipe(debounceTime(500))
        .subscribe(() =>
          localStorage.setItem(
            this.expandFormKey,
            JSON.stringify(this.formGroup.getRawValue())
          )
        )
    );

    const storedData = localStorage.getItem(this.expandFormKey);
    if (storedData) {
      this.formGroup.patchValue(JSON.parse(storedData));
      Object.values(this.formGroup.controls).forEach((control) =>
        control.markAsDirty()
      );
    }
  }

  expand() {
    const subject = this.formGroup.controls['subject'].value;
    this.formGroup.markAsPristine();

    this.cyto.startBatch();

    this.subscriptions.add(
      this.ketoRead
        .expand(
          this.formGroup.getRawValue().namespace,
          this.formGroup.getRawValue().object,
          this.formGroup.getRawValue().relation,
          this.formGroup.getRawValue().max_depth
        )
        .pipe(finalize(() => this.cyto.endBatch()))
        .subscribe((data: ExpandTree) => {
          this.cyto.elements().remove();
          this.cyto.json({ elements: this.expandTreeToNodes(data) });
          this.filterBySubject(subject);

          this.cyto.layout(this.layoutConfig).run();
          /* this.cyto
            .elements('node[type="subject-set"]')
            .layout(this.layoutConfig)
            .run(); */
          //this.cyto.fit(undefined, 75);
        })
    );
  }

  filterBySubject(subject: string) {
    if (subject.length > 0) {
      const node = this.cyto.$id(subject);
      const newData = node.union(node.predecessors());
      const unwantedData = this.cyto.elements().not(newData);

      this.cyto.remove(unwantedData);
    }
  }

  expandTreeToNodes(
    tree: ExpandTree,
    parent?: { id: string; relation: string }
  ): cytoscape.ElementDefinition[] {
    const data: cytoscape.ElementDefinition[] = [];
    if (tree.subject_id) {
      const id = tree.subject_id;
      // NODE
      data.push({
        data: { id: id, label: id, type: 'subject', weight: 1 },
      });
      if (parent) {
        // EDGE
        data.push({
          data: {
            id: `${id}${parent.id}${parent.relation}`,
            source: parent.id,
            target: id,
            label: parent.relation,
          },
        });
      }
    }

    if (tree.subject_set) {
      const id = parent
        ? `${tree.subject_set.namespace}:${tree.subject_set.object}#${tree.subject_set.relation}`
        : 'root';
      const nodeLabel = `${tree.subject_set.namespace}:${tree.subject_set.object}#${tree.subject_set.relation}`;
      const relation = tree.subject_set.relation;

      if (parent) {
        // NODE
        data.push({
          data: {
            id: id,
            label: id,
            relation: tree.subject_set.relation,
            weight: 2,
            type: 'subject-set',
          },
        });
        // EDGE artificial 1 depth more
        /* data.push({
          data: {
            id: `${id}-inv`,
            type: 'invisible',
          },
        });
        data.push({
          data: {
            id: `${id}${parent.id}${parent.relation}-inv`,
            source: parent.id,
            target: `${id}-inv`,
            type: 'invisible',
          },
        });
        data.push({
          data: {
            id: `inv-${id}${parent.id}${parent.relation}`,
            source: `${id}-inv`,
            target: id,
            type: 'invisible',
          },
        }); */
        data.push({
          data: {
            id: `${id}${parent.id}${parent.relation}`,
            source: parent.id,
            target: id,
            type: 'hierarchy',
          },
        });
      } else {
        // ROOT NODE
        data.push({
          data: {
            id: id,
            label: nodeLabel,
            relation: tree.subject_set.relation,
            type: 'root',
          },
        });
      }

      if (tree.children) {
        tree.children.forEach((child) =>
          this.expandTreeToNodes(child, { id, relation }).forEach((t) =>
            data.push(t)
          )
        );
      }
    }

    return data;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
