import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SettingsService } from '../services/settings.service';
import { debounceTime, Subscription } from 'rxjs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnDestroy {
  private readonly specialTokens = [
    '$subject_id',
    '$object',
    '$relation',
    '$namespace',
    '$subject_set.relation',
    '$subject_set.object',
    '$subject_set.namespace',
  ];
  readonly templateTooltipText = `Available variables: ${this.specialTokens.join(
    ','
  )}`;
  readonly readFormControl = new FormControl('', {
    nonNullable: true,
  });
  readonly writeFormControl = new FormControl('', {
    nonNullable: true,
  });
  readonly subjectTemplateFormControl = new FormControl('', {
    nonNullable: true,
  });
  readonly subjectSetTemplateFormControl = new FormControl('', {
    nonNullable: true,
  });

  private readonly debounceTime = 500;
  private readonly subscriptions = new Subscription();

  constructor(private readonly settingsService: SettingsService) {
    this.readFormControl.patchValue(settingsService.getReadUrl());
    this.writeFormControl.patchValue(settingsService.getWriteUrl());
    this.subjectTemplateFormControl.patchValue(
      settingsService.getSubjectIdTemplate()
    );
    this.subjectSetTemplateFormControl.patchValue(
      settingsService.getSubjectSetTemplate()
    );

    this.subscriptions.add(
      this.readFormControl.valueChanges
        .pipe(debounceTime(this.debounceTime))
        .subscribe((value) => {
          settingsService.setReadUrl(value);
        })
    );

    this.subscriptions.add(
      this.writeFormControl.valueChanges
        .pipe(debounceTime(this.debounceTime))
        .subscribe((value) => {
          settingsService.setWriteUrl(value);
        })
    );

    this.subscriptions.add(
      this.subjectTemplateFormControl.valueChanges
        .pipe(debounceTime(this.debounceTime))
        .subscribe((value) => {
          settingsService.setSubjectIdTemplate(value);
        })
    );

    this.subscriptions.add(
      this.subjectSetTemplateFormControl.valueChanges
        .pipe(debounceTime(this.debounceTime))
        .subscribe((value) => {
          settingsService.setSubjectSetTemplate(value);
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
