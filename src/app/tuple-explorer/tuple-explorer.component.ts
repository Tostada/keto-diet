import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  InternalRelationTuple,
  RelationQuery,
  SubjectSet,
} from '@ory/keto-client';
import { KetoReadService } from '../services/keto-read.service';
import { BehaviorSubject, Subject, Subscription, debounceTime } from 'rxjs';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';
import { SettingsService } from '../services/settings.service';
import { MatFormField } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-tuple-explorer',
  templateUrl: './tuple-explorer.component.html',
  styleUrls: ['./tuple-explorer.component.scss'],
})
export class TupleExplorerComponent implements OnDestroy {
  readonly namespaceKey = 'query_form';

  dataSource$ = new BehaviorSubject<{
    tuples: InternalRelationTuple[];
    nextPage?: string;
  }>({ tuples: [] });

  rawData = true;
  rawDataColumns = ['namespace', 'object', 'relation', 'subject_id'];
  prettyPrintColumns = ['relations'];
  displayedColumns: string[] = this.rawDataColumns;

  subject = true;
  subjectIdTemplate = '';
  subjectSetTemplate = '';

  focusedFields = new Set<MatInput>();

  readonly subjectSetForm = new FormGroup<SubjectSetForm>({
    namespace: new FormControl('', {
      nonNullable: true,
    }),
    object: new FormControl('', { nonNullable: true }),
    relation: new FormControl('', { nonNullable: true }),
  });

  readonly relationQueryForm = new FormGroup<RelationTupleForm>({
    namespace: new FormControl('', {
      nonNullable: true,
    }),
    object: new FormControl('', { nonNullable: true }),
    relation: new FormControl('', { nonNullable: true }),
    subject_id: new FormControl('', { nonNullable: true }),
    subject_set: this.subjectSetForm,
  });

  lastPage = 1;
  pageIndex = 0;
  memoizedTotal = 0;

  subscriptions = new Subscription();

  constructor(
    private readonly ketoReaderService: KetoReadService,
    private readonly settingsService: SettingsService
  ) {
    this.subjectSetForm.disable();
    this.subjectSetForm.addValidators(
      this.emailConditionallyRequiredValidator.bind(this)
    );

    const formValue = localStorage.getItem(this.namespaceKey);
    if (formValue) this.relationQueryForm.patchValue(JSON.parse(formValue));
    this.subscriptions.add(
      this.relationQueryForm.valueChanges
        .pipe(debounceTime(500))
        .subscribe(() =>
          localStorage.setItem(
            this.namespaceKey,
            JSON.stringify(this.relationQueryForm.getRawValue())
          )
        )
    );

    this.subjectIdTemplate = settingsService.getSubjectIdTemplate();
    this.subjectSetTemplate = settingsService.getSubjectSetTemplate();
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  query() {
    this.pageIndex = 0;
    this.lastPage = 1;
    this.memoizedTotal = 0;
    this.queryPage(this.lastPage);
  }

  queryPage(page?: number) {
    this.ketoReaderService
      .getRelationTuples(this.relationQueryForm.value as RelationQuery, {
        pageToken: `${page}`,
      })
      .subscribe((data) => {
        const tuples = data.relation_tuples ?? [];
        const nextPage = data.next_page_token;
        if (nextPage) {
          this.lastPage = Math.max(this.lastPage, parseInt(nextPage));
        }

        this.dataSource$.next({ tuples, nextPage });
      });
  }

  formatSubjectSet(subjectSet: SubjectSet): string {
    return `${subjectSet.namespace}:${subjectSet.object}#${subjectSet.relation}`;
  }

  toggleColumns(rawData: boolean) {
    if (rawData) {
      this.displayedColumns = this.rawDataColumns;
    } else {
      this.displayedColumns = this.prettyPrintColumns;
    }
  }

  focusFormField(field: MatFormField, matInput: MatInput) {
    (field._control.stateChanges as Subject<void>).complete();

    matInput ? (field._control = matInput) : '';
    field['_initializeControl']();
    field['_updateFocusState']();
    this.focusedFields.add(matInput);
  }

  unfocusFormField(field: MatFormField, matInput: MatInput, event: FocusEvent) {
    this.focusedFields.delete(matInput);
    if (this.focusedFields.size === 0) {
      field['_updateFocusState']();
    }
  }

  emailConditionallyRequiredValidator(
    formGroup: AbstractControl
  ): ValidationErrors | null {
    if (formGroup.enabled) {
      return formGroup.value.namespace &&
        formGroup.value.object &&
        formGroup.value.relation
        ? null
        : {
            myEmailFieldConditionallyRequired: true,
          };
    }
    return null;
  }

  toggleSubject(value: boolean) {
    if (value) {
      this.relationQueryForm.controls.subject_set?.disable({
        emitEvent: false,
      });
      this.relationQueryForm.controls.subject_id?.enable({ emitEvent: false });
      this.relationQueryForm.updateValueAndValidity();
    } else {
      this.relationQueryForm.controls.subject_set?.enable({ emitEvent: false });
      this.relationQueryForm.controls.subject_id?.disable({ emitEvent: false });
      this.relationQueryForm.updateValueAndValidity();
    }

    this.subject = value;
  }

  onPageChange(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.queryPage(this.pageIndex + 1);
  }

  totalDataLength({
    tuples,
    nextPage,
  }: {
    tuples: InternalRelationTuple[];
    nextPage?: string;
  }) {
    const extra = nextPage ? 1 : tuples.length;
    const calculatedTotal = (this.lastPage - 1) * 100 + extra;
    this.memoizedTotal = Math.max(this.memoizedTotal, calculatedTotal);
    return this.memoizedTotal;
  }
}

interface RelationTupleForm {
  namespace: FormControl<string>;
  object?: FormControl<string | undefined>;
  relation?: FormControl<string | undefined>;
  subject_id?: FormControl<string | undefined>;
  subject_set?: FormGroup<SubjectSetForm>;
}

interface SubjectSetForm {
  namespace: FormControl<string>;
  object: FormControl<string>;
  relation: FormControl<string>;
}
