import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TupleExplorerComponent } from './tuple-explorer.component';

describe('TupleExplorerComponent', () => {
  let component: TupleExplorerComponent;
  let fixture: ComponentFixture<TupleExplorerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TupleExplorerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TupleExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
