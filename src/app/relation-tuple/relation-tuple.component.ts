import { Component, Input } from '@angular/core';
import { InternalRelationTuple } from '@ory/keto-client';

@Component({
  selector: 'app-relation-tuple',
  templateUrl: './relation-tuple.component.html',
  styleUrls: ['./relation-tuple.component.scss'],
})
export class RelationTupleComponent {
  _relationTuple: InternalRelationTuple | undefined;
  _subjectTemplate: string = '';
  _subjectSetTemplate: string = '';
  _subjectTemplateTokens: string[] = [];
  _subjectSetTemplateTokens: string[] = [];
  private readonly specialTokens = [
    '$subject_id',
    '$object',
    '$relation',
    '$namespace',
    '$subject_set.relation',
    '$subject_set.object',
    '$subject_set.namespace',
  ];
  private readonly splitRegex =
    /(\$subject_id|\$object|\$relation|\$namespace|\$subject_set.relation|\$subject_set.object|\$subject_set.namespace)/;
  tokens: string[] = [];

  @Input() set subjectTemplate(input: string) {
    this._subjectTemplate = input;
    this._subjectTemplateTokens = input.split(this.splitRegex);
    this.formatText();
  }
  @Input() set subjectSetTemplate(input: string) {
    this._subjectSetTemplate = input;
    this._subjectSetTemplateTokens = input.split(this.splitRegex);
    this.formatText();
  }

  @Input('relationTuple')
  get relationTuple(): InternalRelationTuple | undefined {
    return this._relationTuple;
  }

  set relationTuple(input: InternalRelationTuple | undefined) {
    this._relationTuple = input;
    this.formatText();
  }
  displayText = '';
  displayTextTokens = [];

  formatText() {
    if (this._relationTuple?.subject_id) {
      this.tokens = this._subjectTemplateTokens;
      return;
    }

    if (this._relationTuple?.subject_set) {
      this.tokens = this._subjectSetTemplateTokens;
      return;
    }
  }

  hydrateToken(token: string): string {
    const tuple = this._relationTuple;
    if (!tuple) return token;
    return token
      .replaceAll('$subject_id', tuple.subject_id ?? token)
      .replaceAll('$object', tuple.object)
      .replaceAll('$relation', tuple.relation)
      .replaceAll('$namespace', tuple.namespace)
      .replaceAll('$subject_set.relation', tuple.subject_set?.relation ?? token)
      .replaceAll('$subject_set.object', tuple.subject_set?.object ?? token)
      .replaceAll(
        '$subject_set.namespace',
        tuple.subject_set?.namespace ?? token
      );
  }

  specialToken(token: string): boolean {
    return this.specialTokens.includes(token);
  }
}
