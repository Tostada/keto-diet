import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelationTupleComponent } from './relation-tuple.component';

describe('RelationTupleComponent', () => {
  let component: RelationTupleComponent;
  let fixture: ComponentFixture<RelationTupleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelationTupleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RelationTupleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
