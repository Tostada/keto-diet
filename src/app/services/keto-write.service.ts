import { Injectable, OnDestroy } from '@angular/core';
import { Configuration, WriteApi } from '@ory/keto-client';
import { Subscription, tap } from 'rxjs';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root',
})
export class KetoWriteService implements OnDestroy {
  // ToDo have provider that creates new instance whenever the URL changes
  private readonly subscriptions = new Subscription();
  private ketoWriteApi: WriteApi;
  constructor(private readonly settingsService: SettingsService) {
    this.ketoWriteApi = new WriteApi(
      new Configuration({
        basePath: settingsService.getWriteUrl(),
      })
    );

    this.subscriptions.add(
      this.settingsService
        .writeUrlChanges()
        .pipe(
          tap((url) => {
            this.ketoWriteApi = new WriteApi(
              new Configuration({
                basePath: url,
              })
            );
          })
        )
        .subscribe()
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
