import { Injectable, OnDestroy } from '@angular/core';
import {
  Configuration,
  ExpandTree,
  GetCheckResponse,
  GetRelationTuplesResponse,
  InternalRelationTuple,
  ReadApi,
  RelationQuery,
} from '@ory/keto-client';
import { from, Observable, Subscription, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { SettingsService } from './settings.service';

@Injectable({
  providedIn: 'root',
})
export class KetoReadService implements OnDestroy {
  private ketoReadApi: ReadApi;
  private readonly subscriptions = new Subscription();
  constructor(private readonly settingsService: SettingsService) {
    this.ketoReadApi = new ReadApi(
      new Configuration({
        basePath: settingsService.getReadUrl(),
      })
    );

    this.subscriptions.add(
      this.settingsService
        .readUrlChanges()
        .pipe(
          tap((url) => {
            this.ketoReadApi = new ReadApi(
              new Configuration({
                basePath: url,
              })
            );
          })
        )
        .subscribe()
    );
  }

  getRelationTuples(
    relation: RelationQuery,
    opts?: {
      pageSize?: number;
      pageToken?: string;
    }
  ): Observable<GetRelationTuplesResponse> {
    return from(
      this.ketoReadApi.getRelationTuples(
        relation.namespace,
        opts?.pageToken,
        opts?.pageSize,
        this.sanitizeField(relation.object),
        this.sanitizeField(relation.relation),
        this.sanitizeField(relation.subject_id),
        this.sanitizeField(relation.subject_set?.namespace),
        this.sanitizeField(relation.subject_set?.object),
        this.sanitizeField(relation.subject_set?.relation),
        {}
      )
    ).pipe(map((res) => res.data));
  }

  check(relation: InternalRelationTuple): Observable<GetCheckResponse> {
    return from(
      this.ketoReadApi.getCheck(
        relation.namespace,
        relation.object,
        relation.relation,
        relation.subject_id,
        relation.subject_set?.namespace,
        relation.subject_set?.object,
        relation.subject_set?.relation
      )
    ).pipe(map((res) => res.data));
  }

  expand(
    namespace: string,
    object: string,
    relation: string,
    maxDepth: number = 3
  ): Observable<ExpandTree> {
    return from(
      this.ketoReadApi.getExpand(namespace, object, relation, maxDepth)
    ).pipe(map((res) => res.data));
  }

  private sanitizeField(field: string | undefined): string | undefined {
    if (field) return field;

    return undefined;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
