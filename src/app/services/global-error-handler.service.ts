import { ErrorHandler, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private _snackBar: MatSnackBar) {}

  handleError(error: any) {
    this._snackBar.open(error?.message ?? 'Unknown error', 'Dismiss', {
      duration: 0,
      verticalPosition: 'top',
      horizontalPosition: 'center',
    });

    console.error('Error from global error handler', error);
  }
}
