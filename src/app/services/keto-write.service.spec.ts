import { TestBed } from '@angular/core/testing';

import { KetoWriteService } from './keto-write.service';

describe('KetoWriteService', () => {
  let service: KetoWriteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KetoWriteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
