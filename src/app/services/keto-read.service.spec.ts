import { TestBed } from '@angular/core/testing';

import { KetoReadService } from './keto-read.service';

describe('KetoReadService', () => {
  let service: KetoReadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KetoReadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
