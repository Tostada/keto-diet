import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  static DEFAULT_SUBJECT_TEMPLATE = `Identity $subject_id has $relation on $object of type $namespace`;
  static DEFAULT_SUBJECT_SET_TEMPLATE = `Whoever has $subject_set.relation on $subject_set.object of type $subject_set.namespace also has $relation on $object of type $namespace`;
  static DEFAULT_READ_URL = 'http://localhost:4466';
  static DEFAULT_WRITE_URL = 'http://localhost:4467';

  private readonly readUrlKey = 'REST_READ_URL';
  private readonly writeUrlKey = 'REST_WRITE_URL';
  private readonly subjectIdTemplateKey = 'SUBJECT_TEMPLATE';
  private readonly subjectSetTemplateKey = 'SUBJECT_SET_TEMPLATE';

  private readonly readUrl$;
  private readonly writeUrl$;

  constructor() {
    this.readUrl$ = new BehaviorSubject<string>(this.getReadUrl());
    this.writeUrl$ = new BehaviorSubject<string>(this.getWriteUrl());
  }

  setReadUrl(url: string) {
    localStorage.setItem(this.readUrlKey, url);
    this.readUrl$.next(url);
  }

  getReadUrl(): string {
    return (
      localStorage.getItem(this.readUrlKey) ?? SettingsService.DEFAULT_READ_URL
    );
  }

  readUrlChanges(): Observable<string> {
    return this.readUrl$;
  }

  setWriteUrl(url: string) {
    localStorage.setItem(this.writeUrlKey, url);
  }

  getWriteUrl(): string {
    return (
      localStorage.getItem(this.writeUrlKey) ??
      SettingsService.DEFAULT_WRITE_URL
    );
  }

  writeUrlChanges(): Observable<string> {
    return this.writeUrl$;
  }

  setSubjectIdTemplate(template: string) {
    localStorage.setItem(this.subjectIdTemplateKey, template);
  }

  getSubjectIdTemplate(): string {
    return (
      localStorage.getItem(this.subjectIdTemplateKey) ??
      SettingsService.DEFAULT_SUBJECT_TEMPLATE
    );
  }

  setSubjectSetTemplate(template: string) {
    localStorage.setItem(this.subjectSetTemplateKey, template);
  }

  getSubjectSetTemplate(): string {
    return (
      localStorage.getItem(this.subjectSetTemplateKey) ??
      SettingsService.DEFAULT_SUBJECT_SET_TEMPLATE
    );
  }
}
